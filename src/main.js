import Vue from 'vue'
import App from './App.vue'
import style from './assets/scss/style.scss'
import router from './router'

Vue.config.productionTip = false

// make v-focus usable in all components
Vue.directive('scroll', {
    inserted: function (el, binding) {
        const onScrollCallback = binding.value
        window.addEventListener('scroll', () => onScrollCallback())
    }
})

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')


