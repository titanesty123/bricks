export default {
  methods: {
    createdCookieWizard(
      value = {},
      domain = '.web-dev.ddos-guard.net',
      time = 3600,
    ) {
      this.$cookies.set('newWizard', value, {
        path: '/',
        domain,
        maxAge: time,
      });
    },
    createdCookieAffiliate(value = {}) {
      const time = 1209600;
      const domain = process.env.affiliateCookieDomain;

      this.$cookies.set('referrerId', value, {
        domain,
        maxAge: Number(time),
      });
    },
  },
};
