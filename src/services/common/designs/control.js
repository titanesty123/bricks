import {data, filters} from './const';

export default class DesignsApiService {
    static getData() {
        return data
    }

    static getFourData() {
        return data.slice(0, 4);
    }

    static getFilters() {
        return filters
    }
    static getDataById(id) {
        return data.find(item => item.id === +id);
    }
    static getDataDefault() {
        return filters.find(el => el.code === 'all')
    }
}

