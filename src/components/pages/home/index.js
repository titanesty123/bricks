import HomePrimary from './Primary.vue'
import HomeFeedback from './Feedback.vue'
import HomeDream from './Dream.vue'
import HomeServices from './Services.vue'
import HomeExamples from './Examples.vue'
import HomeVideoAbout from './VideoAbout.vue'
import HomeDesign from './Design.vue'
import HomeStages from './Stages.vue'
import HomeAreas from './Areas.vue'

export {
    HomePrimary,
    HomeFeedback,
    HomeDream,
    HomeServices,
    HomeExamples,
    HomeVideoAbout,
    HomeDesign,
    HomeStages,
    HomeAreas,
}
